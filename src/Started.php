<?php

namespace ServiceCore\Shield;

use ServiceCore\Timer\Started as Timer;

class Started extends Shield
{
    /**
     * @var  int  the shield time in milliseconds
     */
    private $time;

    public function __construct(Timer $timer, int $time)
    {
        $this->timer = $timer;
        $this->time  = $time;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function stop(): Stopped
    {
        if (0 < ($remaining = $this->getRemainingMilliseconds())) {
            \usleep(\ceil($remaining * 1000));
        }

        return new Stopped($this->timer->stop());
    }

    private function getRemainingMilliseconds(): float
    {
        return $this->time - ($this->timer->diff() * 1000);
    }
}
