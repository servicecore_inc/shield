<?php

namespace ServiceCore\Shield;

use ServiceCore\Timer\Stopped as Timer;

class Stopped extends Shield
{
    public function __construct(Timer $timer)
    {
        $this->timer = $timer;
    }

    public function reset(): Ready
    {
        return new Ready();
    }
}
