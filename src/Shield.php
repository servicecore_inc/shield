<?php

namespace ServiceCore\Shield;

use ServiceCore\Timer\Timer;

abstract class Shield
{
    /** @var Timer */
    protected $timer;

    public function getTimer(): Timer
    {
        return $this->timer;
    }
}
