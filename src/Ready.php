<?php

namespace ServiceCore\Shield;

use ServiceCore\Timer\Ready as Timer;

class Ready extends Shield
{
    public function __construct()
    {
        $this->timer = new Timer();
    }

    public function start(int $time): Started
    {
        return new Started($this->timer->start(), $time);
    }
}
