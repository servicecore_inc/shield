# Changelog

All notable changes to this project will be documented in this file, in reverse chronological order by release.

## 2.0.1.
### Added
- PHP8.1 support

## 2.0.0.
### Added
- PHP8.0 support

## 1.0.0.
### Added
- Initial library commit
