<?php

namespace ServiceCore\Shield\Test;

use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Shield\Ready as ReadyShield;
use ServiceCore\Shield\Stopped as StoppedShield;
use ServiceCore\Timer\Stopped as Timer;

class StoppedTest extends TestCase
{
    public function testConstructSetsProperties(): void
    {
        $now    = \microtime(true);
        $timer  = new Timer($now - 1, $now);
        $shield = new StoppedShield($timer);
        $class  = new ReflectionClass($shield);

        $property = $class->getProperty('timer');
        $property->setAccessible(true);

        $this->assertSame($timer, $property->getValue($shield));
    }

    public function testGetTimerReturnsTimer(): void
    {
        $now    = \microtime(true);
        $timer  = new Timer($now - 1, $now);
        $shield = new StoppedShield($timer);
        $class  = new ReflectionClass($shield);

        $property = $class->getProperty('timer');
        $property->setAccessible(true);
        $property->setValue($shield, $timer);

        $this->assertSame($timer, $shield->getTimer());
    }

    public function testResetReturnsShield(): void
    {
        $now    = \microtime(true);
        $timer  = new Timer($now - 1, $now);
        $shield = new StoppedShield($timer);

        $this->assertInstanceOf(ReadyShield::class, $shield->reset());
    }
}
