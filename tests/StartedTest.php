<?php

namespace ServiceCore\Shield\Test;

use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Shield\Started as StartedShield;
use ServiceCore\Shield\Stopped as StoppedShield;
use ServiceCore\Timer\Started as Timer;

class StartedTest extends TestCase
{
    public function testConstructSetsProperties(): void
    {
        $timer  = new Timer();
        $time   = 1;
        $shield = new StartedShield($timer, $time);
        $class  = new ReflectionClass($shield);

        $property = $class->getProperty('timer');
        $property->setAccessible(true);

        $this->assertSame($timer, $property->getValue($shield));

        $property = $class->getProperty('time');
        $property->setAccessible(true);

        $this->assertEquals($time, $property->getValue($shield));
    }

    public function testGetTimeReturnsTime(): void
    {
        $time   = 1;
        $shield = new StartedShield(new Timer(), $time);
        $class  = new ReflectionClass($shield);

        $property = $class->getProperty('time');
        $property->setAccessible(true);

        $this->assertEquals($time, $shield->getTime());
    }

    public function testGetTimerReturnsTimer(): void
    {
        $timer  = new Timer();
        $shield = new StartedShield($timer, 1);
        $class  = new ReflectionClass($shield);

        $property = $class->getProperty('timer');
        $property->setAccessible(true);

        $this->assertSame($timer, $shield->getTimer());
    }

    public function testStopWaitsAndReturnsShield(): void
    {
        $time = 500;

        // get the start time in milliseconds
        $start = \microtime(true) * 1000;

        // create a "started" shield and immediately "stop" it
        $shield = (new StartedShield(new Timer(), $time))->stop();

        // get the stop time in milliseconds
        $stop = \microtime(true) * 1000;

        $this->assertInstanceOf(StoppedShield::class, $shield);
        $this->assertGreaterThanOrEqual($time, $stop - $start);
    }
}
