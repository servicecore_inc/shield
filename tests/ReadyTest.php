<?php

namespace ServiceCore\Shield\Test;

use PHPUnit\Framework\TestCase;
use ReflectionClass;
use ServiceCore\Shield\Ready as Shield;
use ServiceCore\Shield\Started;
use ServiceCore\Timer\Ready as Timer;

class ReadyTest extends TestCase
{
    public function testGetTimerReturnsTimer(): void
    {
        $timer  = new Timer;
        $shield = new Shield();
        $class  = new ReflectionClass($shield);

        $property = $class->getProperty('timer');
        $property->setAccessible(true);
        $property->setValue($shield, $timer);

        $this->assertSame($timer, $shield->getTimer());
    }

    public function testStartReturnsShield(): void
    {
        $ready = new Shield();

        $this->assertInstanceOf(Started::class, $ready->start(1));
    }
}
