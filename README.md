# Shield

A constant response time library.

[Timing attacks](https://en.wikipedia.org/wiki/Timing_attack) are a common threat vector for online services. Using a constant code execution-time greatly reduces the risk. 

The shield libary will sleep the application for the difference between the _actual_ execution time and the _desired_ execution time:

```
use ServiceCore\Shield\Ready as Shield;

// ... in a controller or resource somewhere

// instantiate a new shield
$shield = new Shield();

// start the shield with a 500 millisecond *desired* execution time
$shield = $shield->start(500);

// ... do something sensitive like authenticate a password

// stop the shield...
// this will sleep the application for the difference between the *actual* execution 
//     time and the *desired* execution time
//
$shield = $shield->stop();
```

## Usage

A shield has three states: _ready_, _started_, and _stopped_.

You can instantiate a `Ready` shield:

```
use ServiceCore\Shield\Ready as Shield;

$shield = new Shield();
```

You can start a `Ready` shield with a _desired_ execution time in _milliseconds_:

```
// ... continued from the example above

$shield = $shield->start(500);
```

The `start()` method will return a `Started` shield.

You can stop a `Started` shield:

```
// ... continued from the example above

$shield = $shield->stop();
```

The `stop()` method will sleep for the difference between the _actual_ execution time and the _desired_ execution time. The _actual_ execution time is considered to be the time between the calls to the shield's `start()` method and `stop()` method. 

The `stop()` method will return a `Stopped` shield.

Finally, you can reset a `Stopped` shield:

```
// ... continued from the example above

$shield = $shield->reset();
```

The `reset()` method will return a `Ready` shield.


## Version

### 0.1.1, March 7, 2016

* Fix dependency on `obg/timer` from `dev-master` to `~0.1`.

### 0.1.0, March 7, 2016

* Initial release
